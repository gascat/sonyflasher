#!/bin/bash

zenity --question --text="Would you like to run a local session?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash zenity.sh)
else
	command=$(/bin/bash zenitylocal.sh)
fi
