#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xz2flash.sh &)
else
	command=$(rm -r /root/local/H8216_Customized_UK_1313-4679_52.0.A.3.163_R5B)
	zenity --error --text="please close the window"
fi

/bin/bash xz2again.sh

sleep 5
