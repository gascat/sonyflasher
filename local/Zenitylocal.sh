#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "XZ1" FALSE "XZ1_Compact" FALSE "XZ2" FALSE "XZ2_Compact" FALSE "XZ2_Compact_Dual" FALSE "XA1" FALSE "XA_Ultra" FALSE "XA2" FALSE "XA2_Dual" FALSE "XZ3" FALSE "XZ3_Dual" FALSE "L2"  FALSE "XZ_Premuim" FALSE "Z5_Dual");
echo $ans
if [ $ans == "XZ1" ]; then
	/bin/bash xz1.sh 
elif [ $ans == "XZ2" ]; then
	/bin/bash xz2.sh
elif [ $ans == "XA1" ]; then
	/bin/bash xa1.sh
elif [ $ans == "XA2" ]; then
	/bin/bash xa2.sh
elif [ $ans == "XZ3" ]; then
	/bin/bash xz3.sh
elif [ $ans == "XZ3_Dual" ]; then
	/bin/bash xz3d.sh
elif [ $ans == "XZ1_Compact" ]; then
	/bin/bash xz1c.sh
elif [ $ans == "XA_Ultra" ]; then
	/bin/bash xau.sh
elif [ $ans == "XZ2_Compact" ]; then
	/bin/bash xz2c.sh
elif [ $ans == "XZ_Premuim" ]; then
	/bin/bash xz_premium.sh
elif [ $ans == "XZ2_Compact_Dual" ]; then
	/bin/bash xz2cd.sh
elif [ $ans == "XA2_Dual" ]; then
	/bin/bash xa2d.sh
elif [ $ans == "L2" ]; then
	/bin/bash l2.sh
elif [ $ans == "Z5_Dual" ]; then
	/bin/bash z5d.sh
else 
	zenity --error ==text "Unknown Device"
fi
