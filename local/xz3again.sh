#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xz3flash.sh &)
else
	command=$(rm -r /root/local/H8416_Customized_UK_1316-4520_52.0.A.3.163_R15B)
	zenity --error --text="please close the window"
fi

/bin/bash xz3again.sh

sleep 5
