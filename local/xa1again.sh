#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xa1flash.sh &)
else
	command=$(rm -r /root/local/G3121_Customized_UK_1307-5194_48.1.A.2.73_R3B)
	zenity --error --text="please close the window"
fi

/bin/bash xa1again.sh

sleep 5
