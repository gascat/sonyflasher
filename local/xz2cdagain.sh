#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xz2cdflash.sh &)
else
	command=$(rm -r /root/local/H8324_Customized UK_1313-5469_52.0.A.3.163_R2B)
	zenity --error --text="please close the window"
fi

/bin/bash xz2cdagain.sh

sleep 5
