#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xz1cflash.sh &)
else
	command=$(rm -r /root/local/G8441_Customized_UK_1310-6856_47.1.A.16.20_R18B)
	zenity --error --text="please close the window"
fi

/bin/bash xz1cagain.sh

sleep 5
