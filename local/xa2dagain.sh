#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xa2dflash.sh &)
else
	command=$(rm -r /root/local/H4113_Customized UK_1313-0771_50.1.A.13.123_R5A)
	zenity --error --text="please close the window"
fi

/bin/bash xa2dagain.sh

sleep 5
