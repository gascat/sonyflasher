#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash l2flash.sh &)
else
	command=$(rm -r /root/local/H3311_Customized_UK_1312-2021_49.0.A.6.56_R9A)
	zenity --error --text="please close the window"
fi

/bin/bash l2again.sh

sleep 5
