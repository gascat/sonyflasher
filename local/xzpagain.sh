#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xzpflash.sh &)
else
	command=$(rm -r /root/local/G8141_Customized_UK_1308-5320_47.2.A.2.33_R3C)
	zenity --error --text="please close the window"
fi

/bin/bash xzpagain.sh

sleep 5
