#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xz3dflash.sh &)
else
	command=$(rm -r /root/local/H9436_Customized UK_1316-4519_52.0.A.3.163_R11B)
	zenity --error --text="please close the window"
fi

/bin/bash xz3dagain.sh

sleep 5
