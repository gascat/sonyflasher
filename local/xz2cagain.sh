#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xz2cflash.sh &)
else
	command=$(rm -r /root/local/H8314_Customized_UK_1313-5398_52.0.A.3.163_R3B)
	zenity --error --text="please close the window"
fi

/bin/bash xz2cagain.sh

sleep 5
