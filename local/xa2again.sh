#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xa2flash.sh &)
else
	command=$(rm -r /root/local/H3113_Customized_UK_1312-3737_50.1.A.13.83_R20A)
	zenity --error --text="please close the window"
fi

/bin/bash xa2again.sh

sleep 5
