#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash z5dflash.sh &)
else
	command=$(rm -r /root/local/E6633_Customized_CE1_1298-7210_32.2.A.5.11_R7C)
	zenity --error --text="please close the window"
fi

/bin/bash z5dagain.sh

sleep 5
