#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xz1flash.sh &)
else
	command=$(rm -r /root/local/G8341_Customized_UK_1310-6360_47.1.A.16.20_R23B)
	zenity --error --text="please close the window"
fi

/bin/bash xz1again.sh

sleep 5
