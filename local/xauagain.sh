#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash xauflash.sh &)
else
	command=$(rm -r /root/local/F3211_Customized UK_1302-5546_36.1.A.1.106_R2B)
	zenity --error --text="please close the window"
fi

/bin/bash xauagain.sh

sleep 5
