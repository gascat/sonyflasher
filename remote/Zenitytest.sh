#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "XZ1" FALSE "XZ2" FALSE "XA1" FALSE "XA2" FALSE "XZ2" FALSE "XZ3" FALSE "Z5_Compact" FALSE "Z5_Dual" FALSE "XZ2_Compact" FALSE "XZ_Premuim");
echo $ans
if [ $ans == "XZ1" ]; then
	/bin/bash xz1.sh 
elif [ $ans == "XZ2" ]; then
	/bin/bash xz2.sh
elif [ $ans == "XA1" ]; then
	/bin/bash xa1.sh
elif [ $ans == "XA2" ]; then
	/bin/bash/xa2.sh
elif [ $ans == "XZ3" ]; then
	/bin/bash xz3.sh
elif [ $ans == "Z5" ]; then
	/bin/bash z5.sh
elif [ $ans == "Z5_Compact" ]; then
	/bin/bash/ z5_compact.sh
elif [ $ans == "Z5_Dual" ]; then
	/bin/bash z5_dual.sh
elif [ $ans == "XZ2_Compact" ]; then
	/bin/bash xz2_compact.sh
elif [ $ans == "XZ_Premuim" ]; then
	/bin/bash/xz_premium.sh
else 
	zenity --error ==text "Unknown Device"
fi
