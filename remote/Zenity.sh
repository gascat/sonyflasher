#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "XZ1" FALSE "XZ2" FALSE "XA1" FALSE "XA2");
echo $ans
if [ $ans == "XZ1" ]; then
	/bin/bash xz1.sh 
elif [ $ans == "XZ2" ]; then
	/bin/bash xz2.sh
elif [ $ans == "XA1" ]; then
	/bin/bash xa1.sh
elif [ $ans == "XA2" ]; then
	/bin/bash/xa2.sh
else 
	zenity --error ==text "Unknown Device"
fi
